# jmmc-oidb-kubernetes


kustomize build overlays/public/ | kubectl apply -f -
  or 
kustomize build overlays/beta/ | kubectl apply -f -
  and 
the rest for admins

You will have to create configmap:
since last versions, only jetty/xml must be edited:
$ kubectl create configmap oidb-preprod-existdb-cfg --from-file=../jmmc-oidb-docker/oidb-existdb/etc/jetty/jetty.xml
$ kubectl edit configmap oidb-preprod-existdb-cfg


